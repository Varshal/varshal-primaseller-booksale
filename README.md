How to compile :
1. Clone this repository into Eclipse.
2. Enter command line arguments from Run Configurations -> Arguments (Please follow rules for defining command line arguments as show in Parameters section) and run.

Parameters :
The project requires 2 fixed parameters which are --books and --sales, both of which specifies the file path for Book and Sales respectively.

	Rules for parameters :
		1. First command line argument is the path of CSV file containing details for books which is defined as --books, this parameter is required.
		2. Second command line argument is the pathe of CSV file containing details for sales which is defined as --sales, this parameter is required.
		3. Third command line argument is the number of top selling books, which is defined as --top_selling_books, this is optional parameter.
		4. Forth command line argument is the number of top customers, which is --top_customers, this is optional parameter.
		5. Fifth command line argument is the date of sale, which is --sales_on_date, this is optional parameter.
		
Order of parameters does not matter here.