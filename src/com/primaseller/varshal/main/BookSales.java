package com.primaseller.varshal.main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.primaseller.varshal.model.Book;
import com.primaseller.varshal.model.PaymentMethod;
import com.primaseller.varshal.model.Sales;

public class BookSales {

	int topSellingBooksCount;
	int topCustomersCount;
	Date saleOnDate;
	boolean showTopSellingBooks;
	boolean showTopCustomers;
	boolean showSaleOnDate;
	SimpleDateFormat sdf;
	Map<String, Double> saleByDate = new HashMap<>();
	Map<String, Double> topCustomers = new HashMap<>();
	static String bookFilePath;
	static String salesFilePath;

	public BookSales() {

		showTopCustomers = false;
		showTopSellingBooks = false;
		showSaleOnDate = false;
		sdf = new SimpleDateFormat("yyyy-MM-dd");

	}

	public static void main(String[] args) {

		if (args.length < 2) {
			throw new IllegalArgumentException("Please provide at least 2 parameters.");
		}

		BookSales bookSales = new BookSales();

		bookSales.processArgs(args);

		
		Map<String, Book> bookDetails = bookSales.processBooks(bookFilePath);

		List<Sales> salesList = bookSales.processSales(salesFilePath, bookDetails);

		Map<String, Double> booksPurchasedHistory = bookSales.createAndSortMap(salesList);
		bookSales.saleByDate = bookSales.sortMapByValue(bookSales.saleByDate);
		bookSales.topCustomers = bookSales.sortMapByValue(bookSales.topCustomers);

		bookSales.showDetails(booksPurchasedHistory, bookSales.topCustomers, bookSales.saleByDate);

	}

	/**
	 * This method will create and sort the map from list of sales that we
	 * previously created...
	 * 
	 * @param salesList
	 * @return
	 */

	private Map<String, Double> createAndSortMap(List<Sales> salesList) {
		Map<String, Double> booksPurchasedHistory = new HashMap<>();
		for (Sales sale : salesList) {
			List<Map<String, Double>> booksPurchased = sale.getBooksPurchased();

			for (Map<String, Double> bookList : booksPurchased) {
				for (Map.Entry<String, Double> pair : bookList.entrySet()) {
					if (!booksPurchasedHistory.containsKey(pair.getKey())) {
						booksPurchasedHistory.put(pair.getKey(), pair.getValue());
					} else {
						String key = pair.getKey();
						Double existingValue = booksPurchasedHistory.get(key);
						Double newValue = pair.getValue();
						if (newValue > existingValue) {
							booksPurchasedHistory.put(key, newValue);
						}
					}
				}
			}
		}
		return sortMapByValue(booksPurchasedHistory);
	}

	/**
	 * This method sorts the the map by value
	 * 
	 * @param mapToSortByValue
	 * @return
	 */
	private Map<String, Double> sortMapByValue(Map<String, Double> mapToSortByValue) {
		return mapToSortByValue.entrySet().stream().sorted((Map.Entry.<String, Double> comparingByValue().reversed()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}

	/**
	 * This method processes args provided and decide what to display
	 * 
	 * @param args
	 * @return
	 */
	private void processArgs(String[] args) {

		int k = 0;

		if (args.length < 2) {
			throw new IllegalArgumentException("You need to provide file path for books and sales file.");
		} else {
			for (int i = 0; i < args.length; i++) {
				if (args[i].startsWith("--books")) {
					bookFilePath = args[i].split("=")[1];
					k++;
				} else if (args[i].contains("--sales_on_date")) {
					showSaleOnDate = true;
					try {
						saleOnDate = sdf.parse(args[i].split("=")[1]);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} else if (args[i].startsWith("--sales")) {
					salesFilePath = args[i].split("=")[1];
					k++;
				} else if (args[i].startsWith("--top_selling_books")) {
					showTopSellingBooks = true;
					topSellingBooksCount = Integer.parseInt(args[i].split("=")[1]);
				} else if (args[i].startsWith("--top_customers")) {
					showTopCustomers = true;
					topCustomersCount = Integer.parseInt(args[i].split("=")[1]);
				}
			}
			if (k < 2) {
				throw new IllegalArgumentException("You need to provide file path for books and sales file.");
			}
		}
	}

	/**
	 * This method displays data according to the conditions based on args
	 * provided...
	 * 
	 * @param bookMap
	 * @param customerMap
	 * @param dateMap
	 */
	private void showDetails(Map<String, Double> bookMap, Map<String, Double> customerMap,
			Map<String, Double> dateMap) {

		if (showTopSellingBooks) {
			System.out.print("top_selling_books");
			bookMap.keySet().stream().limit(topSellingBooksCount).forEach(e -> {
				System.out.print("\t" + e);
			});
		} else {
			System.out.print("top_selling_books");
			bookMap.keySet().stream().forEach(e -> {
				System.out.print("\t" + e);
			});
		}

		if (showTopCustomers) {
			System.out.print("\ntop_customers");
			customerMap.keySet().stream().limit(topCustomersCount).forEach(e -> {
				System.out.print("\t" + e);
			});
		} else {
			System.out.print("\ntop_customers");
			customerMap.keySet().stream().forEach(e -> {
				System.out.print("\t" + e);
			});
		}

		if (showSaleOnDate) {
			System.out.print("\nsales_on_date\t" + sdf.format(saleOnDate) + "\t" + dateMap.get(sdf.format(saleOnDate)));
		} else {
			System.out.print("\nsales_on_date");
			dateMap.entrySet().forEach(e -> {
				System.out.println("\t" + e.getKey() + "\t" + e.getValue());
			});
		}

	}

	/**
	 * This method reads book-csv-file and populate data into appropriate Map
	 * <String,Book>.
	 * 
	 * @param bookFilePath
	 * @return
	 */

	private Map<String, Book> processBooks(String bookFilePath) {

		String line = null;
		Scanner scanner = null;
		int index = 0;
		Map<String, Book> bookDetails = new HashMap<>();
		try(BufferedReader bookReader = new BufferedReader(new FileReader(bookFilePath))) {

			while ((line = bookReader.readLine()) != null) {
				Book book = new Book();
				scanner = new Scanner(line);
				scanner.useDelimiter(",");
				while (scanner.hasNext()) {
					String data = scanner.next();
					if (index == 0)
						book.setBookId(data);
					else if (index == 1)
						book.setBookTitle(data);
					else if (index == 2)
						book.setBookAuthor(data);
					else if (index == 3)
						book.setBookPrice(Double.parseDouble(data));
					else
						System.out.println("invalid data::" + data);
					index++;
				}
				index = 0;
				bookDetails.put(book.getBookId(), book);
			}

		} catch (IOException e) {
			System.out.println("Error while reading books-csv-file");
			e.printStackTrace();
		}

		return bookDetails;
	}

	/**
	 * This method read sales-csv-file and populate data into appropriate list
	 * of Sales.
	 * 
	 * @param salesFilePath
	 * @param bookDetails
	 * @return
	 */
	private List<Sales> processSales(String salesFilePath, Map<String, Book> bookDetails) {

		String line = null;
		Scanner scanner = null;
		int index = 0;
		List<Sales> salesList = new ArrayList<>();

		try(BufferedReader salesReader = new BufferedReader(new FileReader(salesFilePath))) {
			

			while ((line = salesReader.readLine()) != null) {
				Sales sale = new Sales();
				scanner = new Scanner(line);
				scanner.useDelimiter(",");
				List<Map<String, Double>> booksPurchasedList = new ArrayList<>();
				while (scanner.hasNext()) {
					String data = scanner.next();
					if (index == 0)
						sale.setSaleDate(sdf.parse(data));
					else if (index == 1)
						sale.setBuyerEmail(data);
					else if (index == 2)
						sale.setPaymentMethod(PaymentMethod.valueOf(data));
					else if (index == 3) {
						sale.setBooksSold(Integer.parseInt(data));
					} else if (index >= 4) {
						String arr[] = data.split(";");
						Map<String, Double> booksPurchasedMap = new HashMap<>();
						Double totalPrice = Integer.parseInt(arr[1]) * bookDetails.get(arr[0]).getBookPrice();
						booksPurchasedMap.put(arr[0], totalPrice);
						if (!topCustomers.containsKey(sale.getBuyerEmail())) {
							topCustomers.put(sale.getBuyerEmail(), totalPrice);
						} else {
							Double existingValue = topCustomers.get(sale.getBuyerEmail());
							topCustomers.put(sale.getBuyerEmail(), existingValue + totalPrice);
						}

						if (!saleByDate.containsKey(sdf.format(sale.getSaleDate()))) {
							saleByDate.put(sdf.format(sale.getSaleDate()), totalPrice);
						} else {
							Double existingValue = saleByDate.get(sdf.format(sale.getSaleDate()));
							saleByDate.put(sdf.format(sale.getSaleDate()), existingValue + totalPrice);
						}
						booksPurchasedList.add(booksPurchasedMap);
					} else
						System.out.println("invalid data::" + data);
					index++;
				}
				sale.setBooksPurchased(booksPurchasedList);
				index = 0;
				salesList.add(sale);
			}
		} catch (IOException e) {
			System.out.println("Error while reading sales-csv-file");
			e.printStackTrace();
		} catch (ParseException e) {
			System.out.println("Error while parsing date while reading sales-csv-file");
			e.printStackTrace();
		}

		return salesList;
	}

}
