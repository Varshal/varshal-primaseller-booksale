package com.primaseller.varshal.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Sales {

	private Date saleDate;
	private String buyerEmail;
	private PaymentMethod paymentMethod;
	private int booksSold;
	private List<Map<String, Double>> booksPurchased;
	/**
	 * @return the saleDate
	 */
	public Date getSaleDate() {
		return saleDate;
	}
	/**
	 * @param saleDate the saleDate to set
	 */
	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}
	/**
	 * @return the buyerEmail
	 */
	public String getBuyerEmail() {
		return buyerEmail;
	}
	/**
	 * @param buyerEmail the buyerEmail to set
	 */
	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}
	/**
	 * @return the paymentMethod
	 */
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	/**
	 * @param paymentMethod the paymentMethod to set
	 */
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	/**
	 * @return the booksSold
	 */
	public int getBooksSold() {
		return booksSold;
	}
	/**
	 * @param booksSold the booksSold to set
	 */
	public void setBooksSold(int booksSold) {
		this.booksSold = booksSold;
	}
	/**
	 * @return the booksPurchased
	 */
	public List<Map<String, Double>> getBooksPurchased() {
		return booksPurchased;
	}
	/**
	 * @param booksPurchased the booksPurchased to set
	 */
	public void setBooksPurchased(List<Map<String, Double>> booksPurchased) {
		this.booksPurchased = booksPurchased;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Sales [saleDate=" + saleDate + ", buyerEmail=" + buyerEmail + ", paymentMethod=" + paymentMethod
				+ ", booksSold=" + booksSold + ", booksPurchased=" + booksPurchased + "]";
	}	
}