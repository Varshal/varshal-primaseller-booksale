package com.primaseller.varshal.model;

public enum PaymentMethod {

	CASH,
	CARD
}
